#include <iostream>
#include <string>
using namespace std;

int main() {
    string text;
    cout << "Enter text: ";
    getline(cin, text);

    int wordLength;
    string replacement;
    cout << "Enter the length of the word: ";
    cin >> wordLength;
    cout << "Enter substring: "; 
    cin >> replacement;

    string word;
    string newText;
    
    for (int i = 0; i < text.length(); i++) {
        if (text[i] == ' ' || i == text.length() - 1) {
            if (i == text.length() - 1) {
                word += text[i];
            }
            if (word.length() == wordLength) {
                newText += replacement;
            } else {
                newText += word;
            }
            newText += ' ';
            word = "";
        } else {
            word += text[i];
        }
    }

    cout << "Original text: " << text << endl;
    cout << "New text: " << newText << endl;

    return 0;
}